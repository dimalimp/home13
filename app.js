// One----------------
// if "холодно-горячо"

let age = 1980;
let myYear = 1986;
let diff = Math.abs(age - myYear);

if (diff >= 10) {
    console.log("холодно");
} else if(diff >= 6) {
    console.log("теплее");
} else if(diff >= 2) {
    console.log("жарко");
} else {
    console.log("в точку");
}

// Two-------------------------
// if "угадал-не угадал"

age === 1986 ? console.log("угадал") : console.log("не угадал");

// Three-----------------------
// отрицательное число стало положительным

if (age < 0) {
    age = Math.abs(age)
 }

console.log(age)

// Four--------------------------
// диапазон

if (age >= 1838 && age < 1864) {
    console.log("в это время еще был жив Калиновский")
}

// Five--------------------------
// выведение текущей переменной в тексте

console.log(`вы ввели – ${age} год`);

// Six-----------------------------
// меняем значение переменной на строку

let strName = String(age)
console.log(typeof(strName))

// Seven--------------------------------
// вывод последнего или первого символа

const len = strName.length;
len >= 3 ?  console.log(strName[len - 1]) : console.log(strName[0]);

// Eight------------------------------
// найти одну строку в другой

const strOne = "авптма23вы"
const strTwo = "пт"

console.log(strOne.indexOf(strTwo) !== -1)

// Nine--------------------------------
// выводит по одному символу в обратном порядке

const l = strName.length;
for (let i = 1; i <= l; i++) {
    console.log(strName[l - i])
 }

// Ten-------------------------------
// цикл уменьшает или увеличивает на один переменную

while (age !== myYear) {
    console.log(age);
    age < myYear ? age = age + 1 : age = age - 1
}


// Eleven----------------------------
// считает сколько нулей в перменной

let q = 0;
for (let i = 0; i < strName.length; i++) {
    if (strName[i] === "0") {
        q = q + 1
    }
}
console.log(q);

// Thirteen----------------------------
// замена символов

const x = strName.replaceAll("2", "3");
console.log(x)

// Fourteen-------------------------
// заменяет все 0 на 1

let strName = '1900';
let newStrName = "";
for (let i = 0; i < strName.length; i++) {
    strName[i] === "0" ? newStrName = newStrName + 1 : newStrName = newStrName + strName[i]
}
strName = newStrName
console.log(strName)

